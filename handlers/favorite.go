package handlers

import (
	"projectDV-api/models"
	"strconv"
	"strings"

	"github.com/gin-gonic/gin"
)

type FavoriteHandler struct {
	fs models.FavoriteService
}

func NewFavoriteHandler(fs models.FavoriteService) *FavoriteHandler {
	return &FavoriteHandler{fs}
}

type AddRateReq struct {
	Rate uint
}

func (fh *FavoriteHandler) AddFavoriteRate(c *gin.Context) {
	header := c.GetHeader("Authorization")
	header = strings.TrimSpace(header)
	min := len("Bearer ")
	if len(header) <= min {
		c.Status(401)
		c.Abort()
		return
	}
	token := header[min:]
	user, err := fh.fs.GetUserByToken(token)
	if err != nil {
		c.Status(500)
		return
	}

	GarageIDStr := c.Param("garage_id")
	garageID, err := strconv.Atoi(GarageIDStr)
	if err != nil {
		c.Status(400)
		return
	}

	rateReq := new(AddRateReq)
	if err = c.BindJSON(rateReq); err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
	}

	rate := new(models.Favorite)
	rate.GarageID = uint(garageID)
	rate.UserID = user.ID
	rate.Rate = rateReq.Rate

	err = fh.fs.GiveScoreByMaintenaceIDandUserID(rate)
	if err != nil {
		c.Status(500)
		return
	}
	c.Status(200)
}

func (fh *FavoriteHandler) GetFavoriteRate(c *gin.Context) {

}
