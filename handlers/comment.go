package handlers

import (
	"fmt"
	"projectDV-api/models"
	"strconv"
	"strings"

	"github.com/gin-gonic/gin"
)

type CommentHandler struct {
	cms models.CommentService
}

func NewCommentHandler(cms models.CommentService) *CommentHandler {
	return &CommentHandler{cms}
}

type userReqComment struct {
	Comment string
}

func (cmh *CommentHandler) PostComment(c *gin.Context) {
	header := c.GetHeader("Authorization")
	header = strings.TrimSpace(header)
	min := len("Bearer ")
	if len(header) <= min {
		c.Status(401)
		c.Abort()
		return
	}
	token := header[min:]
	user, err := cmh.cms.GetUserByToken(token)
	if err != nil {
		c.Status(500)
		return
	}

	contentIDStr := c.Param("content_id")
	contentID, err := strconv.Atoi(contentIDStr)
	if err != nil {
		c.Status(400)
		return
	}

	reqComment := new(userReqComment)
	if err := c.BindJSON(reqComment); err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
	}

	comment := new(models.Comments)
	comment.Comment = reqComment.Comment
	comment.ContentID = uint(contentID)
	comment.UserID = user.ID

	err = cmh.cms.PostComments(comment)
	if err != nil {
		c.Status(400)
		return
	}
	c.JSON(200, gin.H{
		"comment": comment.Comment,
	})
}

func (cmh *CommentHandler) ListComment(c *gin.Context) {
	contentIDStr := c.Param("content_id")

	contentID, err := strconv.Atoi(contentIDStr)
	if err != nil {
		c.Status(400)
		return
	}
	fmt.Println(contentID)
	commentList, err := cmh.cms.ListByContentID(uint(contentID))
	if err != nil {
		c.Status(500)
		return
	}

	c.JSON(200, commentList)
}
