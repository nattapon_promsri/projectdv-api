package handlers

import (
	"projectDV-api/models"
	"strings"

	"github.com/gin-gonic/gin"
)

type UserHandler struct {
	us models.UserService
}

func NewUserHandler(us models.UserService) *UserHandler {
	return &UserHandler{us}
}

////////////////////////////

type UserHandlerReq struct {
	Firstname string `json:"firstname"`
	Lastname  string `json:"lastname"`
	Username  string `json:"username"`
	Password  string `json:"password"`
	Role      bool   `json:"role"`
}

func (uh *UserHandler) Signup(c *gin.Context) {
	userData := new(UserHandlerReq)

	if err := c.BindJSON(userData); err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}

	user := new(models.User)
	user.Firstname = userData.Firstname
	user.Lastname = userData.Lastname
	user.Username = userData.Username
	user.Password = userData.Password
	user.Role = userData.Role

	if err := uh.us.SignUp(user); err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
	}
	c.JSON(201, nil)
}

type UserSigninReq struct {
	Username string `json:"username"`
	Password string `json:"password"`
}

func (uh *UserHandler) SignIn(c *gin.Context) {

	userLoginReq := new(UserSigninReq)

	if err := c.BindJSON(userLoginReq); err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
	}

	userLogin := new(models.User)
	userLogin.Username = userLoginReq.Username
	userLogin.Password = userLoginReq.Password

	token, err := uh.us.SignIn(userLogin)
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
	}
	c.JSON(201, gin.H{
		"token": token,
	})
}
func (uh *UserHandler) GetUserData(c *gin.Context) {
	header := c.GetHeader("Authorization")
	header = strings.TrimSpace(header)
	min := len("Bearer ")
	if len(header) <= min {
		c.Status(401)
		c.Abort()
		return
	}

	token := header[min:]
	user, err := uh.us.GetByToken(token)
	if err != nil {
		c.Status(500)
		return
	}
	c.JSON(200, gin.H{
		"firstname": user.Firstname,
		"lastname":  user.Lastname,
		"role":      user.Role,
	})
}
