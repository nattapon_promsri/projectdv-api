package handlers

import (
	"projectDV-api/models"
	"strings"

	"github.com/gin-gonic/gin"
)

type GarageHandler struct {
	gs models.GarageService
}

func NewGarageHandler(gs models.GarageService) *GarageHandler {
	return &GarageHandler{gs}
}

type addGarageReq struct {
	Name        string
	Description string
	Latitude    string
	Longitude   string
}

// type addLoacationReq struct {
// 	Latitude   string
// 	Longitude string
// }

func (gh *GarageHandler) AddGarage(c *gin.Context) {
	header := c.GetHeader("Authorization")
	header = strings.TrimSpace(header)
	min := len("Bearer ")
	if len(header) <= min {
		c.Status(401)
		c.Abort()
		return
	}
	token := header[min:]
	user, err := gh.gs.GetUserByToken(token)
	if err != nil {
		c.Status(500)
		return
	}

	garageReq := new(addGarageReq)
	if err = c.BindJSON(garageReq); err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
	}

	garage := new(models.Garage)
	garage.Name = garageReq.Name
	garage.Description = garageReq.Description
	garage.Latitude = garageReq.Latitude
	garage.Longitude = garageReq.Longitude
	garage.UserID = user.ID

	err = gh.gs.AddGarage(garage)
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
	}

	// locationReq := new(models.Location)
	// locationReq.Latitude = garageReq.Location.Latitude
	// locationReq.Longitude = garageReq.Location.Longitude
	// locationReq.GarageID = garage.ID

	// err = gh.ls.AddLocation(locationReq)
	// if err != nil {
	// 	c.JSON(400, gin.H{
	// 		"message": err.Error(),
	// 	})
	// }

	c.JSON(201, nil)
}

func (gh *GarageHandler) ListGarage(c *gin.Context) {
	garage_list, err := gh.gs.ListGarage()
	if err != nil {
		c.JSON(500, gin.H{
			"message": "can't find server",
		})
		return
	}
	c.JSON(200, garage_list)
}

func (gh *GarageHandler) GetOneGarage(c *gin.Context) {
	header := c.GetHeader("Authorization")
	header = strings.TrimSpace(header)
	min := len("Bearer ")
	if len(header) <= min {
		c.Status(401)
		c.Abort()
		return
	}
	token := header[min:]
	user, err := gh.gs.GetUserByToken(token)
	if err != nil {
		c.Status(500)
		return
	}
	oneGarage, err := gh.gs.GetOneGarage(user.ID)
	if err != nil {
		c.JSON(500, gin.H{
			"message": "can't find server",
		})
		return
	}
	c.JSON(200, oneGarage)
}
