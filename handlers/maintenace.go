package handlers

import (
	"projectDV-api/models"

	"github.com/gin-gonic/gin"
)

type MaintenaceHandler struct {
	ms models.MaintenaceService
}

func NewMaintenaceHandler(ms models.MaintenaceService) *MaintenaceHandler {
	return &MaintenaceHandler{ms}
}

func (mh *MaintenaceHandler) ListMaintenace(c *gin.Context) {
	maintenace_list, err := mh.ms.ListMaintenace()
	if err != nil {
		c.JSON(500, gin.H{
			"message": "can't find server",
		})
		return
	}
	c.JSON(200, maintenace_list)
}

type addNameReq struct {
	Name string `json:"name"`
	Photo string `json:"photo"`
}

func (mh *MaintenaceHandler) AddName(c *gin.Context) {
	nameReq := new(addNameReq)
	if err := c.BindJSON(nameReq); err != nil {
		c.JSON(400, gin.H{
			"message": "error",
		})
		return
	}
	maintenace := new(models.Maintenace)
	maintenace.Name = nameReq.Name
	maintenace.Photo = nameReq.Photo
	if err := mh.ms.CreateMaintenaceName(maintenace); err != nil {
		c.JSON(500, gin.H{
			"message": "err",
		})
		return
	}

	c.JSON(200, maintenace)
}

