package handlers

import (
	"projectDV-api/models"
	"strconv"
	"strings"

	"github.com/gin-gonic/gin"
)

type ContentHandler struct {
	cs models.ContentService
}

func NewContentHandler(cs models.ContentService) *ContentHandler {
	return &ContentHandler{cs}
}

type reqHeader struct {
	Head        string
	Description string
}

func (ch *ContentHandler) PostContent(c *gin.Context) {
	header := c.GetHeader("Authorization")
	header = strings.TrimSpace(header)
	min := len("Bearer ")
	if len(header) <= min {
		c.Status(401)
		c.Abort()
		return
	}
	token := header[min:]
	user, err := ch.cs.GetUserByToken(token)
	if err != nil {
		c.Status(500)
		return
	}

	maintenaceIDStr := c.Param("maintenace_id")
	maintenaceID, err := strconv.Atoi(maintenaceIDStr)
	if err != nil {
		c.Status(400)
		return
	}

	userReqHeader := new(reqHeader)
	if err := c.BindJSON(userReqHeader); err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
	}

	content := new(models.Content)
	content.Head = userReqHeader.Head
	content.Description = userReqHeader.Description
	content.MaintenaceID = uint(maintenaceID)
	content.UserID = user.ID

	err = ch.cs.PostContent(content)
	if err != nil {
		c.Status(400)
		return
	}
	c.Status(200)
}

func (ch *ContentHandler) ListContent(c *gin.Context) {
	maintenaceIDStr := c.Param("maintenace_id")
	maintenaceID, err := strconv.Atoi(maintenaceIDStr)
	if err != nil {
		c.Status(400)
		return
	}
	contentList, err := ch.cs.ListByMaintenaceID(uint(maintenaceID))
	if err != nil {
		c.Status(500)
		return
	}
	c.JSON(200, contentList)
}
