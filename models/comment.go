package models

import (
	// "fmt"

	"fmt"

	"github.com/jinzhu/gorm"
)

type Comments struct {
	gorm.Model
	Comment   string `json:"comment"`
	ContentID uint
	UserID    uint
}

type CommentService interface {
	GetUserByToken(token string) (*User, error)
	PostComments(comments *Comments) error
	ListByContentID(id uint) ([]req, error)
}

type CommentGorm struct {
	db *gorm.DB
}

func NewCommentService(db *gorm.DB) CommentService {
	return &CommentGorm{db}
}

func (cmg *CommentGorm) GetUserByToken(token string) (*User, error) {
	user := new(User)
	err := cmg.db.Where("token = ?", token).First(user).Error
	if err != nil {
		return nil, err
	}
	return user, nil
}

func (cmg *CommentGorm) PostComments(comments *Comments) error {
	return cmg.db.Create(comments).Error
}

type req struct {
	Comment   string
	Firstname string
	Lastname  string
}

func (cmg *CommentGorm) ListByContentID(id uint) ([]req, error) {
	commentList := []req{}
	rows, err := cmg.db.Table("comments").Select("comments.comment,users.firstname,users.lastname").
		Joins("JOIN users ON comments.user_id = users.id").
		Joins("JOIN contents ON comments.content_id = contents.id").
		Where("contents.id = ? ", id).Rows()
	if err != nil {
		fmt.Println(err)
	}
	for rows.Next() {
		res := req{}
		err := rows.Scan(&res.Comment, &res.Firstname, &res.Lastname)
		if err != nil {
			fmt.Println(err)
		}
		commentList = append(commentList, res)
	}
	if err != nil {
		return nil, err
	}
	return commentList, nil

	// if err := cmg.db.Where("content_id = ?", id).Find(&commentList).Error; err != nil {
	// 	return nil, err
	// }
	// return commentList, nil

}
