package models

import "github.com/jinzhu/gorm"

type Content struct {
	gorm.Model
	Head         string `json:"head"`
	Description  string `json:"description"`
	MaintenaceID uint
	UserID       uint
}

type ContentService interface {
	GetUserByToken(token string) (*User, error)
	PostContent(content *Content) error
	ListByMaintenaceID(id uint) ([]Content, error)
	// OneContent(id uint) (*Content, error)
}

type ContentGorm struct {
	db *gorm.DB
}

func NewContentService(db *gorm.DB) ContentService {
	return &ContentGorm{db}
}

///////////////////////////////////////////////////////

func (cg *ContentGorm) GetUserByToken(token string) (*User, error) {
	user := new(User)
	err := cg.db.Where("token = ?", token).First(user).Error
	if err != nil {
		return nil, err
	}
	return user, nil
}

func (cg *ContentGorm) PostContent(content *Content) error {
	return cg.db.Create(content).Error
}

func (cg *ContentGorm) ListByMaintenaceID(id uint) ([]Content, error) {
	contentList := []Content{}
	if err := cg.db.Where("maintenace_id = ?", id).Find(&contentList).Error; err != nil {
		return nil, err
	}
	return contentList, nil

}
