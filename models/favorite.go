package models

import "github.com/jinzhu/gorm"

type Favorite struct {
	gorm.Model
	GarageID uint
	UserID   uint
	Rate     uint `json:"rate"`
}

type FavoriteService interface {
	GetUserByToken(token string) (*User, error)
	GiveScoreByMaintenaceIDandUserID(rate *Favorite) error
}

type FavoriteGorm struct {
	db *gorm.DB
}

func NewFavoriteService(db *gorm.DB) FavoriteService {
	return &FavoriteGorm{db}
}

func (fg *FavoriteGorm) GetUserByToken(token string) (*User, error) {
	user := new(User)
	err := fg.db.Where("token = ?", token).First(user).Error
	if err != nil {
		return nil, err
	}
	return user, nil
}

func (fg *FavoriteGorm) GiveScoreByMaintenaceIDandUserID(rate *Favorite) error {
	return fg.db.Create(rate).Error
}