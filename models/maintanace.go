package models

import "github.com/jinzhu/gorm"

type Maintenace struct {
	gorm.Model
	Name string
	Photo string
}
type MaintenaceService interface {
	ListMaintenace() ([]Maintenace, error)
	CreateMaintenaceName(maintenace *Maintenace) error
}
type maintenaceGorm struct {
	db *gorm.DB
}

func NewMaintenaceService(db *gorm.DB) MaintenaceService {
	return &maintenaceGorm{db}
}

/////////////////////////////////////////
func (mg *maintenaceGorm) ListMaintenace() ([]Maintenace, error) {
	maintenace_data := []Maintenace{}
	if err := mg.db.Find(&maintenace_data).Error; err != nil {
		return nil, err
	}
	return maintenace_data, nil
}

func (mg *maintenaceGorm) CreateMaintenaceName(maintenace *Maintenace) error {
	return mg.db.Create(maintenace).Error
}
