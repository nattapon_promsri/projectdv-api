package models

import "github.com/jinzhu/gorm"

type Garage struct {
	gorm.Model
	Name        string `json:"name"`
	Description string `json:"description"`
	Latitude    string `json:"latitude"`
	Longitude   string `json:"longitude"`
	UserID      uint
}

type GarageService interface {
	GetUserByToken(token string) (*User, error)
	ListGarage() ([]Garage, error)
	AddGarage(temp *Garage) error
	GetOneGarage(id uint) ([]Garage, error)
}

type GarageGorm struct {
	db *gorm.DB
}

func NewGarageService(db *gorm.DB) GarageService {
	return &GarageGorm{db}
}

func (gg *GarageGorm) GetUserByToken(token string) (*User, error) {
	user := new(User)
	err := gg.db.Where("token = ?", token).First(user).Error
	if err != nil {
		return nil, err
	}
	return user, nil
}

func (gg *GarageGorm) ListGarage() ([]Garage, error) {
	garage_data := []Garage{}
	if err := gg.db.Find(&garage_data).Error; err != nil {
		return nil, err
	}
	return garage_data, nil
}
func (gg *GarageGorm) AddGarage(temp *Garage) error {
	return gg.db.Create(temp).Error
}

func (gg *GarageGorm) GetOneGarage(id uint) ([]Garage, error) {
	garage := []Garage{}
	err := gg.db.Where("user_id = ?", id).Find(&garage).Error
	if err != nil {
		return nil, err
	}
	return garage, nil
}
