package models

import (
	"github.com/jinzhu/gorm"
	"golang.org/x/crypto/bcrypt"

	"crypto/rand"
	"encoding/base64"
)

const cost = 4

type User struct {
	gorm.Model
	Firstname string `json:"firstname"`
	Lastname  string `json:"lastname"`
	Username  string `gorm:"unique_index;not null" json:"firstname"`
	Password  string `gorm:"not null" json:"lastname"`
	Token     string `gorm:"unique_index" json:"token"`
	Role      bool   `json:"role"`
	Filename  string `json:"filename"`
}

type UserService interface {
	SignUp(user *User) error
	SignIn(user *User) (string, error)
	GetByToken(token string) (*User, error)
}

type UserGorm struct {
	db *gorm.DB
}

func NewUserService(db *gorm.DB) UserService {
	return &UserGorm{db}
}

///////////////////////////////

func (ug *UserGorm) SignUp(temp *User) error {
	user := new(User)
	user.Firstname = temp.Firstname
	user.Lastname = temp.Lastname
	user.Username = temp.Username
	user.Password = temp.Password
	user.Role = temp.Role

	hash, err := bcrypt.GenerateFromPassword([]byte(user.Password), cost)
	if err != nil {
		return err
	}
	user.Password = string(hash)

	token, err := GetToken()
	if err != nil {
		return err
	}

	user.Token = token
	temp.Token = token
	return ug.db.Create(user).Error
}

func GetToken() (string, error) {
	b := make([]byte, 32)

	_, err := rand.Read(b)
	if err != nil {
		return "", err
	}
	return base64.URLEncoding.EncodeToString(b), nil
}

///////////////////////////////////////

func (ug *UserGorm) SignIn(user *User) (string, error) {

	found := new(User)
	err := ug.db.Where("username = ?", user.Username).First(&found).Error
	if err != nil {
		return "", err
	}
	err = bcrypt.CompareHashAndPassword([]byte(found.Password), []byte(user.Password))
	if err != nil {
		return "", err
	}
	token, err := GetToken()
	if err != nil {
		return "", err
	}
	err = ug.db.Model(&User{}).
		Where("id = ?", found.ID).
		Update("token", token).Error
	if err != nil {
		return "", err
	}
	return token, nil

}

func (ug *UserGorm) GetByToken(token string) (*User, error) {
	user := new(User)
	err := ug.db.Where("token = ?", token).First(user).Error
	if err != nil {
		return nil, err
	}
	return user, nil
}
