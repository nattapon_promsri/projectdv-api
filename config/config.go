package config

import (
	"os"
)

type Config struct {
	Mode       string
	Connection string
}

func Load() *Config {
	conf := &Config{}
	conf.Mode = os.Getenv("MODE")
	if conf.Mode == "" {
		conf.Mode = "dev"
	}
	conf.Connection = os.Getenv("DB_URL")
	if conf.Connection == "" {
		conf.Connection = "root:password@tcp(127.0.0.1:3306)/appdb?parseTime=true"
	}

	return conf
}