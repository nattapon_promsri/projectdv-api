package main

import (
	"log"
	"projectDV-api/config"
	"projectDV-api/handlers"
	"projectDV-api/models"
	"time"

	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
)

func main() {

	conf := config.Load()

	db, err := gorm.Open("mysql", conf.Connection)

	// db, err := gorm.Open("mysql", "root:password@tcp(127.0.0.1:3306)/appdb?parseTime=true")
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	if err := db.AutoMigrate(
		&models.Maintenace{}, 
		&models.User{}, 
		&models.Content{}, 
		&models.Comments{}, 
		&models.Garage{}, 
		&models.Favorite{},
		).Error; err != nil {
		log.Fatal(err)
	}

	///////service-models////////
	ms := models.NewMaintenaceService(db)
	us := models.NewUserService(db)
	cs := models.NewContentService(db)
	cms := models.NewCommentService(db)
	gs := models.NewGarageService(db)
	fs := models.NewFavoriteService(db)
	/////////////////////////////
	///////handlers/////
	mh := handlers.NewMaintenaceHandler(ms)
	uh := handlers.NewUserHandler(us)
	ch := handlers.NewContentHandler(cs)
	cmh := handlers.NewCommentHandler(cms)
	gh := handlers.NewGarageHandler(gs)
	fh := handlers.NewFavoriteHandler(fs)
	////////////////////////////////

	r := gin.Default()
	r.Use(cors.New(cors.Config{
		AllowAllOrigins:  true,
		AllowMethods:     []string{"GET", "PUT", "PATCH", "POST", "DELETE", "HEAD"},
		AllowHeaders:     []string{"Origin", "Content-Length", "Content-Type", "Authorization"},
		AllowCredentials: true,
		MaxAge:           12 * time.Hour,
	}))
	// if conf.Mode != "dev" {
    //     gin.SetMode(gin.ReleaseMode)
    // }
	/////path/////
	r.POST("/signup", uh.Signup)          //everyone
	r.POST("/signin", uh.SignIn)          //user
	r.GET("/getUserData", uh.GetUserData) //user(ทำ UserProfile)

	r.GET("/maintenace", mh.ListMaintenace) //everyone
	r.POST("/maintenaceName", mh.AddName)    //admin

	r.GET("/content/:maintenace_id", ch.ListContent)  //everyone
	r.POST("/content/:maintenace_id", ch.PostContent) //user

	r.GET("/comment/:content_id", cmh.ListComment)  //everyone
	r.POST("/comment/:content_id", cmh.PostComment) //user

	r.GET("/garage", gh.ListGarage)         //everyone
	r.GET("/getOneGarage", gh.GetOneGarage) // user(ที่เป็น role อู่รถ)
	r.POST("/garage", gh.AddGarage)         //user(ที่เป็น role อู่รถ)

	r.GET("/favorite:garage_id", fh.GetFavoriteRate)   //everyone
	r.POST("/favorite/:garage_id", fh.AddFavoriteRate) //user

	r.Run()

}
